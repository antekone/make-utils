#!/usr/bin/env bash

BUILD_DIR="$HOME/temp/build"

realpath="`/usr/bin/readlink $0`"
if [ "$realpath" == "" ]; then
    mydir=`/usr/bin/dirname $0`
else
    mydir=`/usr/bin/dirname $realpath`
fi

templates=$mydir/templates

if [ ! -d $templates ]; then
    echo "No access to templates dir: $templates, can't continue."
    exit 1
fi

silent_pushd() {
    pushd "$1" >/dev/null 2>/dev/null
}

silent_popd() {
    popd >/dev/null 2>/dev/null
}

exit_with_retcode() {
    if [ "$ok" == "0" ]; then
        exit 1
    else
        exit 0
    fi
}

init_rust() {
    /usr/bin/m4 $templates/rust-make.m4 > .make.sh
    chmod +x .make.sh
    echo Configured Rust.
    let ok=1
}

create_build_dir_error() {
    echo "Error: create your build directory first:"
    echo ""
    echo "    mkdir $BUILD_DIR/$basename"
}

locate_ninja_binary() {
    ninja_binary="`which ninja 2>/dev/null`"
    if [ ! -f "$ninja_binary" ]; then
        ninja_binary="`which ninja-build 2>/dev/null`"
        if [ ! -f "$ninja_binary" ]; then
            echo "Error: tried to set up Ninja project, but can't find Ninja binary."
            echo "Error: failing."
            let ok=0
            return
        fi
    fi
}

has_sln_file() {
    bdir="$1"
    out="`ls -1 $bdir/*.sln 2>/dev/null | wc -l | egrep -Eo '[0-9]+'`"
    if [ "$out" == "0" ]; then
        return 1
    else
        return 0
    fi
}

init_build_sbt() {
    source_dir="$1"

    sbt_client="`which sbt-client`"
    if [ "$sbt_client" == "" ]; then
        echo "Error: sbt-client doesn't seem to exist in your PATH."
        echo "Error: I'll still create the project file, but it probably won't work."
    fi

    if [ "$JAVA_HOME" == "" ]; then
        echo "Error: missing JAVA_HOME environment variable, can't continue."
        exit 1
    fi

    sbt_tool="`which sbt`"
    if [ "$sbt_tool" == "" ]; then
        echo "Error: missing sbt tool. Can't continue."
        exit 1
    fi

    mkdir project >/dev/null 2>/dev/null || true
    echo 'addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")' > project/assembly_.sbt

    [ -f ".jarpath.sh" ] && rm .jarpath.sh

    /usr/bin/m4 -DPWD="$source_dir" $templates/sbt-client.m4 > .make.sh

    echo "- Running initial build..."

    unset _JAVA_OPTIONS
    $sbt_tool assembly
    if [ "$?" != "0" ]; then
        echo "Error: sbt assembly didn't return success. Please fix your environment,"
        echo "Error: clean the build, and try again."
        exit 1
    fi

    find target -name '*-assembly-*.jar' -print0 | xargs -0 echo -n > /tmp/temp.file
    if [ ! -f /tmp/temp.file ]; then
        echo "Error: something is wrong with the build. The assembly jar hasn't been found."
        echo "Error: please fix it manually."
        exit 1
    fi

    jarpath="`cat /tmp/temp.file`"
    rm /tmp/temp.file >/dev/null 2>/dev/null || true

    echo "export JARPATH=\"$jarpath\"" > .jarpath.sh

    /usr/bin/m4 -DPWD="$source_dir" $templates/scala-run.sh.m4 > .run.sh

    chmod +x .run.sh
    chmod +x .make.sh

    echo -e "target\n.*.sh\nproject/project\nproject/target" > .gitignore

    git init
    git add .
    git commit -m "Initial commit"

    echo "Configured sbt-client."
    let ok=1
}

init_autotools_oob() {
    source_dir="$1"
    build_dir="$2"

    if [ -f "$build_dir/Makefile" ]; then
        # Autotools already initialized
        echo already initialized
    else
        echo "Initializing Autotools project."

        autoreconf_tool="autoreconf"
        [ -f "$build_dir/autotools.sh" ] && source "$build_dir/autotools.sh"
        echo "1. Running $autoreconf_tool -fi"
        /usr/bin/env $autoreconf_tool -fi

        if [ "$?" != "0" ]; then
            echo "Error: 'autoreconf -fi' has failed, can't continue."
            echo ""
            echo "Hint: if you want to use autoreconf in a different version, or"
            echo "if autoreconf is named differently on your system, edit this file:"
            echo ""
            echo "    $build_dir/autotools.sh"
            echo ""
            echo 'and add i.e. `autoreconf_tool="autoreconf-2.13"` directive. This'
            echo "file will be sourced by this build script and the name you've"
            echo "chosen will be used during autoreconf step."
            let ok=0
            exit_with_retcode
        fi

        silent_pushd "$build_dir"
        echo "2. Running configure --prefix $build_dir/.install $configure_options"
        $source_dir/configure --prefix $build_dir/.install $configure_options
        if [ "$?" != "0" ]; then
            echo "Error: 'configure' step has failed, can't continue."
            echo ""
            echo "Hint: if you want to use different options in the configure step,"
            echo "then, edit this file:"
            echo ""
            echo "    $build_dir/autotools.sh"
            echo ""
            echo 'and add i.e. `configure_options="--enable-something"` directive. This'
            echo "file will be sourced by this build script and the command line you've"
            echo "chosen will be used during configure step step."
            let ok=0
            exit_with_retcode
        fi

        silent_popd

        /usr/bin/m4 -DBUILDDIR="$build_dir" -DBUILDTOOL="/usr/bin/env make -C $build_dir" $templates/cmake.m4 > .make.sh
        chmod +x .make.sh

        echo "Configured Autotools."
        let ok=1
    fi
}

init_cmake_cpp() {
    builddir="$1"
    tool=""

    if [ -f "$1/Makefile" ]; then
        # TODO: check if we need BSD make or GNU make?
        makecmd="`which make`"
        tool="make"
    elif [ -f "$1/build.ninja" ]; then
        let ok=1
        locate_ninja_binary
        [ "$ok" == "0" ] && exit_with_retcode
        makecmd="$ninja_binary"
        tool="ninja"
    elif has_sln_file "$1"; then
        let ok=1
        makecmd="`which msbuild.exe`"
        tool="msbuild"
    else
        # If there is no Makefile nor build.ninja, the directory is probably
        # unconfigured. So use Ninja.

        let ok=1
        locate_ninja_binary
        [ "$ok" == "0" ] && exit_with_retcode
        makecmd="$ninja_binary"
        tool="ninja"
    fi

    if [ "$makecmd" == "" ]; then
        echo "Error: couldn't figure out which build tool to use."
        ok=0
        return
    fi

    echo "Using build tool: $makecmd"
    echo "Using build dir: $builddir"

    if [ "$tool" == "make" ]; then
        makecmd="\"$makecmd\" -C $builddir"
        standard_m4=1
    elif [ "$tool" == "ninja" ]; then
        makecmd="\"$makecmd\" -C $builddir"
        standard_m4=1
    elif [ "$tool" == "msbuild" ]; then
        standard_m4=0
        /usr/bin/m4 -DBUILDDIR="$builddir" -DBUILDTOOL="$makecmd" $templates/msbuild.m4 > .make.sh
    else
        echo "Error: unknown build tool: $tool."
        ok=0
        return
    fi

    if [ "$standard_m4" == "1" ]; then
        /usr/bin/m4 -DBUILDDIR="$builddir" -DBUILDTOOL="$makecmd" $templates/cmake.m4 > .make.sh
    fi

    echo -e "#!/usr/bin/env bash\n\ncd \"$BUILD_DIR/$basename\" && ./cpptest" > .run.sh

    chmod +x .make.sh
    chmod +x .run.sh

    echo "Configured C++."
    let ok=1
}

# --- main ---

if [ -f ".make.sh" ]; then
    echo "Build is already configured here. Remove .make.sh and try again."
    exit 1
fi

ok=0

# --- Check for Rust project ---
if [ -f "Cargo.toml" ]; then
    init_rust
    exit_with_retcode
fi

basename="`basename $PWD`"

echo Checking build dir...
# Create build dir if not exists
if [ ! -d "$BUILD_DIR/$basename" ]; then
    mkdir -p "$BUILD_DIR/$basename" || true
fi

# --- Check for CLion C/C++ project --
if [ -d ".idea" -o -d "cmake-build-debug" -o -d "cmake-build-release" ]; then
    test1="`cat .idea/*iml | grep CPP_MODULE`"
    if [ ! "$test1" == "" ]; then
        init_cmake_cpp "cmake-build-debug"
        exit_with_retcode
    fi
fi

# -- Check for normal CMake C++ project --
if [ -f "CMakeLists.txt" ]; then
    source_dir="$PWD"
    if [ -d "$BUILD_DIR/$basename" ]; then
        init_cmake_cpp "$BUILD_DIR/$basename"
        pushd $BUILD_DIR/$basename >/dev/null 2>/dev/null

        if [ ! -f "CMakeCache.txt" ]; then
            cmake -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Debug "$source_dir"
        fi

        popd >/dev/null 2>/dev/null
    else
        create_build_dir_error
        ok=0
    fi

    exit_with_retcode
fi

# -- Check for normal autotools project --
if [ -f "Makefile.am" -a -f "configure.ac" ]; then
    source_dir="$PWD"
    build_dir="$BUILD_DIR/$basename"

    if [ -d "$build_dir" ]; then
        init_autotools_oob "$source_dir" "$build_dir"
    else
        create_build_dir_error
        ok=0
    fi

    exit_with_retcode
fi

# -- Check for Scala/sbt project --
if [ -f "build.sbt" -a -d "src" ]; then
    source_dir="$PWD"

    # Build directory for this project is inside source directory.
    # SBT doesn't allow do do it otherwise?

    init_build_sbt "$source_dir"
    exit_with_retcode
fi

# -- Check for Visual Studio project type 1 --
solutions="`find . -path './M*/*/*.sln'`"
if [ "$solutions" != "" -a "`echo $solutions | wc -l`" == "1" ]; then
    BUILDDIR=`realpath ./Modules/*`
    build_tool="`which msbuild.exe`"
    buildmod="`which buildmod.exe`"

    if [ "$build_tool" == "" ]; then
        echo "Couldn't find msbuild.exe!"
        exit 1
    fi

    if [ "$buildmod" == "" ]; then
        echo "Couldn't find buildmod.exe!"
        exit 1
    fi

    sln="`realpath ./M*/*/*.sln`"
    project_name="`basename ${sln%.*}`"
    /usr/bin/m4 -DBUILDDIR="$BUILDDIR" -DBUILDMOD="$buildmod" -DPROJECT_NAME="$project_name" -DBUILDTOOL="$build_tool" $templates/msbuild_type1.m4 > .make.sh
    echo "Configured MSBUILD type #1 project."
    exit 0
fi

# -- Check for standard Visual Studio project --
solutions="`ls -1 *.sln`"
if [ "$solutions" != "" -a "`echo $solutions | wc -l`" == "1" ]; then
    BUILDDIR="`realpath .`"
    build_tool="`which msbuild.exe`"

    if [ "$build_tool" == "" ]; then
        echo "Couldn't find msbuild.exe!"
        exit 1
    fi

    sln="`realpath *.sln`"
    /usr/bin/m4 -DBUILDDIR="$BUILDDIR" -DBUILDTOOL="$build_tool" $templates/msbuild.m4 > .make.sh
    echo "Configured MSBUILD type #2 project."
    exit 0
fi

if [ "$ok" == "0" ]; then
    echo "Nothing to do."
    exit 1
fi
