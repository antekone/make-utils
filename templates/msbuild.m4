#!/usr/bin/env sh

# --- args ---
export CONFIGURATION=Release # Debug
export SOLUTION="*.sln"
export PLATFORM=x64 # Win32
export TARGET=""
# ------------

export `BUILDDIR'="BUILDDIR"
export `BUILDTOOL'="BUILDTOOL"

build() {
    pushd "`$BUILDDIR'" >/dev/null 2>/dev/null
    if [ "$TARGET" != "" ]; then
        TARGETSTR="//target:$TARGET"
    else
        TARGETSTR=""
    fi

    echo "Building..."
    "`$BUILDTOOL'" //nologo //verbosity:quiet //consoleloggerparameters:NoSummary,ForceNoAlign //p:Platform=$PLATFORM //p:Configuration=$CONFIGURATION $SOLUTION $TARGETSTR |
        sed -ue  "s#\\\#/#g" |
        sed -ue  's#(\([0-9]\+\),\([0-9]\+\))#:\1:\2#g' |
        sed -ue  's#^[a-zA-Z]:/msys64##g' |
        sed -ue  's#^\([a-zA-Z]\):#/\1#g' |
        sed -Eue 's#(.*?)\[.*#\1#g'
    ret=${PIPESTATUS[0]}

    popd "`$BUILDDIR'" >/dev/null 2>/dev/null
    return $ret
}

deploy() {
    true
}

build `$@'

if [ ! "$?" == "0" ]; then
    echo "Build FAILED. Press enter to continue."
    read x
    exit 1
else
    echo "Build OK"
fi

deploy
