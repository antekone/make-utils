#!/usr/bin/env sh

# --- args ---
export CONFIGURATION=Release
export SOLUTION="*.sln"
export PLATFORM=x64
export TARGET=""
# ------------

export `BUILDDIR'="BUILDDIR"
export `BUILDTOOL'="BUILDTOOL"

build() {
    pushd "`$BUILDDIR'" >/dev/null 2>/dev/null
    if [ "$TARGET" != "" ]; then
        TARGETSTR="//target:$TARGET"
    else
        TARGETSTR=""
    fi

    echo "Building..."
    "`$BUILDTOOL'" //nologo //verbosity:quiet //consoleloggerparameters:NoSummary,ForceNoAlign //p:Platform=$PLATFORM //p:Configuration=$CONFIGURATION $SOLUTION $TARGETSTR |
        sed -ue  "s#\\\#/#g" |
        sed -ue  's#(\([0-9]\+\),\([0-9]\+\))#:\1:\2#g' |
        sed -ue  's#^[a-zA-Z]:/msys64##g' |
        sed -ue  's#^\([a-zA-Z]\):#/\1#g' |
        sed -Eue 's#(.*?)\[.*#\1#g'
    ret=${PIPESTATUS[0]}

    popd "`$BUILDDIR'" >/dev/null 2>/dev/null
    return $ret
}

setup_env() {
    export OUTPUT_DIR_PRE="`$BUILDDIR'/../../../$CONFIGURATION/PROJECT_NAME/$PLATFORM"

    changequote([,])
    export OUTPUT_DIR="`realpath $OUTPUT_DIR_PRE`"
    export DLL_PATH="`realpath $OUTPUT_DIR/*.dll`"
    export NAKED_FILENAME="`basename ${DLL_PATH%.*}`"
    export DAT_PATH="`realpath $OUTPUT_DIR/$NAKED_FILENAME.dat`"
    export DAT_FILE="$DAT_PATH"
    export DLL_FILE="$DLL_PATH"
    export NUMBER_PRE="`echo $NAKED_FILENAME | grep -Eo '[[]0-9[]]+' | sed -e 's#^0*##g' | head -n1`"
    export BUILDMOD_NUMBER="$(( $NUMBER_PRE + 1 ))"
    changequote([`])

    true
}

# 0 if OK, 1 if ERROR
validate_env() {
    if [ ! -f "$OUTPUT_DIR/*.dll" ]; then
        return 1
    fi

    return 0
}

post_build_system() {
    [ -f "$DAT_PATH" ] && rm "$DAT_PATH"

    "BUILDMOD" -i "$DLL_PATH" -n $BUILDMOD_NUMBER
    if [ ! "$?" == "0" ]; then
        echo "buildmod FAILed!"
        exit 1
    fi

    if [ -f "$DAT_PATH" ]; then
        post_build
        return
    fi

    echo "POST-BUILD phase failed, buildmod didn't create any output files"
    exit 1
}

post_build() {
    sha1sum "$DAT_FILE"
    true
}

deploy() {
    true
}

clear
build `$@'

if [ ! "$?" == "0" ]; then
    echo "Build FAILED. Press enter to continue."
    read x
else
    setup_env
    if [ validate_env == 1 ]; then
        echo "Build OK, but no output file was found!"
        ls -la "$OUTPUT_DIR"
        exit 1
    fi
    post_build_system

    deploy
    echo "Build OK"
fi
