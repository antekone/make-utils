#!/usr/bin/bash

source "PWD/.jarpath.sh"
unset _JAVA_OPTIONS

"$JAVA_HOME/bin/java" -jar "$JARPATH" "$@"
