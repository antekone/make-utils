#!/usr/bin/env sh

export `BUILDDIR'="BUILDDIR"

build() {
    BUILDTOOL `$@'
}

deploy() {
    true
}

build `$@'

if [ ! "$?" == "0" ]; then
    echo "Build FAILED. Press enter to continue."
    read x
else
    echo "Build OK"
fi

[ ! -f "compile_commands.json" -a -f "$`BUILDDIR'/compile_commands.json" ] && ln -sf "$`BUILDDIR'/compile_commands.json" .

deploy
