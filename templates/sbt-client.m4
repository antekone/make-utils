#!/usr/bin/env sh

unset _JAVA_OPTIONS

export PATH=$JAVA_HOME/bin:$PATH
source "PWD/.jarpath.sh"

deploy() {
    true
}

call_sbt() {
    args="$1"
    sbt-client "$args" | sed -ur "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
    ret="${PIPESTATUS[0]}"
}

if [ "$1" == "update" -o "$1" == "up" ]; then
    echo "Reloading Sbt shell"
    call_sbt reload
    if [ "$ret" != "0" ]; then
        echo "Sbt reload failed"
        exit 1
    fi

    echo "Reload OK"
else
    echo "Reloading Sbt shell"
    call_sbt reload

    echo "Building"
    call_sbt assembly

    if [ "$ret" != "0" ]; then
        echo "Sbt compile failed"
        exit 1
    fi

    deploy
    echo "Build OK"
fi
