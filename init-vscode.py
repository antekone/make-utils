import argparse
import os

class RequirementNotMet(RuntimeError):
    def __init__(self, msg: str):
        self.msg = msg
        RuntimeError.__init__(self, msg)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--cmake-variants', action='store_true', help='generate cmake variants stub')
    return parser.parse_args()


def require_file(filename: str):
    if not os.path.exists(filename):
        raise RequirementNotMet('missing required file: "' + filename + '"')

    if not os.path.isfile(filename):
        raise RequirementNotMet(f'the following item is not a file: "{filename}"')


def require_dir(filename: str):
    if not os.path.exists(filename):
        raise RequirementNotMet('missing required directory: "' + filename + '"')

    if not os.path.isdir(filename):
        raise RequirementNotMet(f'the following item is not a directory: "{filename}"')


def require_not_exists(filename: str):
    if os.path.exists(filename):
        raise RequirementNotMet('filesystem object shouldn\'t exist, but it exists: "' + filename + '"')


def generate_cmake_variants(args):
    require_file("CMakeLists.txt")
    require_not_exists(".vscode/tasks.json")


def main():
    args = parse_args()

    if args.cmake:
        return generate_cmake(args)

    print('Error: nothing to do, consult "-h".')


if __name__ == '__main__':
    try:
        main()
    except RequirementNotMet as e:
        print("Error: requirements not met -- " + e.msg)
