#!/usr/bin/env sh

# --- args ---
export CONFIGURATION=Release
export SOLUTION="*.sln"
export PLATFORM=x64
export TARGET=""
# ------------

export BUILDDIR="vs2019"
export BUILDTOOL="msbuild.exe"

build() {
    pushd "$BUILDDIR" >/dev/null 2>/dev/null
    if [ "$TARGET" != "" ]; then
        TARGETSTR="//target:$TARGET"
    else
        TARGETSTR=""
    fi

    echo "Building VS..."

    "$BUILDTOOL" //nologo //verbosity:quiet //consoleloggerparameters:NoSummary,ForceNoAlign //p:Platform=$PLATFORM //p:Configuration=$CONFIGURATION $SOLUTION $TARGETSTR |
        sed -ue  "s#\\\#/#g" |
        sed -ue  's#(\([0-9]\+\),\([0-9]\+\))#:\1:\2#g' |
        sed -ue  's#^C:/msys64##g' |
        sed -Eue 's#(.*?)\[.*#\1#g'
    ret=${PIPESTATUS[0]}

    popd "$BUILDDIR" >/dev/null 2>/dev/null
    return $ret
}

deploy() {
    cp "Release/cpptest.exe" ../cpptest_vs2019.exe
}

build $@
deploy

if [ ! "$ret" == "0" ]; then
    echo "Build FAILED."
    exit 1
else
    echo "Build OK"
    exit 0
fi
