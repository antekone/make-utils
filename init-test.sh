#!/usr/bin/env sh

realpath="`/usr/bin/readlink $0`"
if [ "$realpath" == "" ]; then
    mydir=`/usr/bin/dirname $0`
else
    mydir=`/usr/bin/dirname $realpath`
fi

init_cpp() {
    if [ -f "test.cpp" -o -f "CMakeLists.txt" ]; then
        echo "Some files already exist in current dir, please remove them first:"
        [ -f "test.cpp" ]       && echo "- test.cpp"
        [ -f "CMakeLists.txt" ] && echo "- CMakeLists.txt"
        exit 1
    fi

    cp -v $mydir/templates/tests/cpp/test.cpp .
    cp -v $mydir/templates/tests/cpp/CMakeLists.txt .

    echo "Created a CPP test."
}

init_c() {
    if [ -f "test.c" -o -f "CMakeLists.txt" ]; then
        echo "Some files already exist in current dir, please remove them first:"
        [ -f "test.c" ]         && echo "- test.c"
        [ -f "CMakeLists.txt" ] && echo "- CMakeLists.txt"
        exit 1
    fi

    cp -v $mydir/templates/tests/c/test.c .
    cp -v $mydir/templates/tests/c/CMakeLists.txt .

    echo "Created a C test."
}

init_rust() {
    cargo="`which cargo`"
    if [ "$cargo" == "" ]; then
        echo "Cargo not found!"
        exit 1
    fi

    $cargo init --bin --vcs none .

    m4 $mydir/templates/rust-make.m4 > .make.sh
    chmod +x .make.sh

    echo "Created a Rust test."
}

init_scala() {
    if [ -f "build.sbt" -o -f "project/assembly_.sbt" ]; then
        echo "There are already some build files in this directory. Please remove them first:"
        [ -f "build.sbt" ]              && echo "- build.sbt"
        exit 1
    fi

    mkdir -p src/main/scala >/dev/null 2>/dev/null || true
    m4 $mydir/templates/build.sbt.m4 > build.sbt
    m4 $mydir/templates/main.scala.m4 > src/main/scala/Main.scala
    echo "Created a Scala test."
}

if [ "$1" == "cpp" ]; then
    init_cpp
elif [ "$1" == "c" ]; then
    init_c
elif [ "$1" == "rust" ]; then
    init_rust
elif [ "$1" == "scala" ]; then
    init_scala
else
    echo "Unknown language selected. Use one of:"
    echo "- cpp"
    echo "- c"
    echo "- rust"
    echo "- scala"
    exit 1
fi
